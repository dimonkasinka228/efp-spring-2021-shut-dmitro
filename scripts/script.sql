DROP DATABASE IF EXISTS `time_organisation`;
DROP TABLE IF EXISTS `activities`;
DROP TABLE IF EXISTS `categories`;
DROP TABLE IF EXISTS `roles`;
DROP TABLE IF EXISTS `task_requests`;
DROP TABLE IF EXISTS `teams`;
DROP TABLE IF EXISTS `user_activity`;
DROP TABLE IF EXISTS `user_team`;
DROP TABLE IF EXISTS `users`;
CREATE DATABASE `time_organisation`;
USE `time_organisation`;


CREATE TABLE `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(40) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `categories` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(40) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `teams` (
  `team_id` int NOT NULL AUTO_INCREMENT,
  `team_name` varchar(45) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(40) NOT NULL,
  `user_last_name` varchar(40) NOT NULL,
  `user_password` varchar(40) NOT NULL,
  `user_login` varchar(40) NOT NULL,
  `user_role_id` int NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login` (`user_login`),
  KEY `users_ibfk_1` (`user_role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `custom_categories` (
	`custom_category_id` int NOT NULL AUTO_INCREMENT, 
	`custom_category_name` varchar(40) NOT NULL, 
    `user_id` int NOT NULL,
    PRIMARY KEY (`custom_category_id`),
    CONSTRAINT `custom_category_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
);
CREATE TABLE `activities` (
  `activity_id` int NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(40) NOT NULL,
  `activity_deadline` datetime DEFAULT NULL,
  `activity_category_id` int DEFAULT NULL,
  `activity_end_time` datetime DEFAULT NULL,
  `custom_category_id` int DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `activities_ibfk_1` (`activity_category_id`),
  CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`activity_category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `activities_ibfk_2` FOREIGN KEY (`custom_category_id`) REFERENCES `custom_categories` (`custom_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user_team` (
  `user_team_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `team_id` int NOT NULL,
  PRIMARY KEY (`user_team_id`),
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `user_team_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `user_team_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user_activity` (
  `user_activity_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `activity_id` int NOT NULL,
  PRIMARY KEY (`user_activity_id`),
  KEY `user_id` (`user_id`),
  KEY `activity_id` (`activity_id`),
  CONSTRAINT `user_activity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `user_activity_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `categories` VALUES (1,'Appointed'),(2,'Important'),(3,'My tasks'),(4,'Requested');
INSERT INTO `roles` VALUES (1,'Администратор'),(3,'Гость'),(2,'Пользователь');
INSERT INTO `users` VALUES (1,'Иван','Иванов','admin','admin',1),(2,'Кирилл','Баранов','user','user',2),(3,'Дмитрий ','Гордон','guest','guest',3);
INSERT INTO `teams` VALUES (1,'Команда 1'),(2,'Команда 2'),(3,'Команда 3'),(4,'Сумашедшие крокодилы'),(5,'Команда 3'),(6,'Команда 5');
INSERT INTO `user_team` VALUES (1,1,1),(2,1,2),(3,2,1),(4,2,4),(5,2,5);
INSERT INTO `activities`(`activity_id`,  `activity_name`,  `activity_deadline`,  `activity_category_id`,  `activity_end_time`) VALUES (9,'Нарисовать шкаф','2021-02-06 00:00:00',2,'2021-03-06 00:00:00'),
	(20,'Написать ТЗ','2021-02-06 00:00:00',2,'2021-03-06 00:00:00'),
    (21,'Выполнить работу','2021-02-06 00:00:00',2,'2021-04-06 00:00:00'),
    (22,'Написать документацию','2021-02-06 00:00:00',2,'2021-04-06 00:00:00'),(23,'Задача админа 2','2021-02-06 00:00:00',3,'2021-04-06 00:00:00'),(24,'Написать письмо ','2021-04-06 00:00:00',1,'2021-06-07 00:00:00'),(25,'Создать класс','2021-04-06 00:00:00',2,NULL),(26,'Тест активности','2021-04-06 00:00:00',2,'2021-06-02 00:00:00'),(27,'Купить кочергу','2021-04-06 00:00:00',2,'2021-06-02 00:00:00'),(28,'Нарисовать кочергу','2021-04-06 00:00:00',1,NULL),(29,'Купить акции Apple','2021-04-06 00:00:00',2,'2021-06-05 00:00:00'),(30,'Собрать шкаф','2021-04-06 00:00:00',2,'2021-06-05 00:00:00'),(31,'Поднять сервер','2021-04-07 00:00:00',2,'2021-06-05 00:00:00'),(32,'Купить маску','2021-04-07 00:00:00',2,NULL),(39,'uhuh','2021-06-11 00:00:00',1,'2021-06-06 00:00:00'),(40,'Первое назначенное задание','2021-06-11 00:00:00',2,'2021-06-04 00:00:00'),(41,'Второе назначенное задание','2021-06-16 00:00:00',1,'2021-06-07 00:00:00'),(42,'Третье задание','2021-06-11 00:00:00',1,'2021-06-07 00:00:00'),(43,'4 задание','2021-06-10 00:00:00',2,'2021-06-04 00:00:00'),(44,'Тестовое задание для юзера','2021-06-10 00:00:00',2,'2021-06-05 00:00:00'),(45,'Первое неважное задание','2021-06-18 00:00:00',1,'2021-06-05 00:00:00'),(46,'Запрошенное задание','2021-06-18 00:00:00',4,NULL),(48,'uhuh','2021-06-11 00:00:00',2,'2021-06-06 00:00:00'),(51,'Тестовое задание для юзера 2','2021-06-11 00:00:00',1,'2021-06-07 00:00:00'),(52,'Тестовое задание для юзера 3','2021-06-18 00:00:00',2,NULL),(53,'Тестовый запрос на задачу от юзера asdf','2021-06-15 00:00:00',3,NULL),(54,'Новый тест после смены папки','2021-06-10 00:00:00',2,NULL),(55,'uhuh','2021-06-09 00:00:00',1,'2021-06-07 00:00:00'),(57,'Я прошу ещё задание','2021-06-08 00:00:00',3,NULL),(59,'запрос нарисовать кочергу','2021-06-08 00:00:00',3,NULL),(60,'Запрос на новое задание','2021-06-09 00:00:00',4,NULL),(61,'Админ дай задачу','2021-06-09 00:00:00',3,NULL),(62,'Give me some task','2021-06-08 00:00:00',3,NULL),(63,'Я прошу ещё задание 1','2021-06-08 00:00:00',4,NULL),(64,'Я прошу ещё задание 2','2021-07-09 00:00:00',4,NULL),(65,'Я прошу ещё задание 3','2021-06-08 00:00:00',4,NULL),(66,'Я тебе даю задание 1 ','2021-06-09 00:00:00',1,'2021-06-07 00:00:00'),(67,'Я даю тебе задание 2','2021-06-09 00:00:00',1,'2021-06-07 00:00:00'),(68,'Я тебе даю задние 3','2021-06-16 00:00:00',1,'2021-06-07 00:00:00'),(69,'Я даю тебе задание 4','2021-06-10 00:00:00',1,'2021-06-07 00:00:00');
INSERT INTO `user_activity` VALUES (39,1,20),(40,1,21),(41,2,22),(42,2,23),(43,2,24),(44,2,22),(45,2,26),(46,2,20),(47,2,20),(48,2,31),(49,1,30),(50,2,29),(51,2,28),(52,2,27),(53,2,26),(54,2,26),(55,2,27),(56,2,30),(57,2,22),(58,2,29),(59,1,31),(60,2,31),(61,2,31),(62,2,31),(63,2,31),(64,2,31),(65,2,31),(66,2,31),(67,2,31),(68,2,31),(69,2,31),(70,2,31),(71,2,31),(72,2,31),(73,2,31),(74,2,31),(75,2,31),(76,2,31),(77,2,32),(84,2,39),(85,2,40),(86,2,41),(87,2,42),(88,2,43),(89,2,44),(90,2,45),(92,2,48),(95,2,51),(96,2,52),(97,2,53),(98,2,54),(99,2,55),(101,2,57),(103,2,59),(104,2,60),(105,2,61),(106,2,62),(107,2,63),(108,2,64),(109,2,65),(110,2,66),(111,2,67),(112,2,68),(113,2,69);
INSERT INTO `custom_categories` VALUES (1, 'Без категории', 2);


