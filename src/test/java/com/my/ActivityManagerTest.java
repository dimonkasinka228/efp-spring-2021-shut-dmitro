package com.my;

import com.my.DB.ActivityManager;
import com.my.DB.DBManager;
import com.my.DB.DBUtils;
import com.my.Util.DBException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.sql.*;

public class ActivityManagerTest {
    private Connection con;
    private MockedStatic<DBUtils> dbUtils;

    @Test
    public void testCompleteActivity() throws DBException, SQLException {
        ActivityManager activityManager = ActivityManager.getInstance();
        DBManager dbManager = DBManager.getInstance();
        //Mockito.when(dbManager.completeActivity(con, "sdf", "sfsd")).thenReturn(1);
        Mockito.when(activityManager.completeActivity("wefe", "rty"))
                .thenReturn(1);
        Assert.assertEquals(1, activityManager.completeActivity("" , ""));

    }

    @Before
    public void setUp() {
        con = Mockito.mock(Connection.class);
        dbUtils = Mockito.mockStatic(DBUtils.class);
        dbUtils.when(DBUtils::getConnection).thenReturn(con);
    }

    @After
    public void tearDown() {
        dbUtils.close();
    }
}
