package com.my;

import com.my.DB.DBManager;
import com.my.DB.DBUtils;
import com.my.DB.Entity.Activity;
import com.my.DB.Entity.CustomCategory;
import com.my.DB.Entity.User;
import com.my.Util.FieldNames;
import com.my.Util.SQLCommands;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.sql.*;
import java.util.List;

public class DBManagerTest {
    private Connection con;
    private Statement stmt;
    private PreparedStatement pstmt;
    private ResultSet rs;
    private MockedStatic<DBUtils> dbUtils;

    @Test
    public void testGetUser() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.FIND_USER_BY_LOGIN);
        Mockito.when(rs.next()).thenReturn(true);
        Mockito.when(rs.getString(FieldNames.USER_LOGIN)).thenReturn("admin");
        DBManager dbManager = DBManager.getInstance();
        User user = dbManager.getUser(con,"admin");
        Assert.assertEquals("admin", user.getUserLogin());
    }

    @Test
    public void testGetAllUsers() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.GET_ALL_USERS);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(rs.getString("login")).thenReturn("admin").thenReturn("client").thenReturn("client2");

        DBManager dbManager = DBManager.getInstance();
        List<User> users = dbManager.getAllUsers(con);
        Assert.assertEquals(3, users.size());
    }

    @Test
    public void testGetCompletedActivities() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.FIND_COMPLETED_ACTIVITIES);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(rs.getString(FieldNames.ACTIVITY_NAME)).thenReturn("activity1").thenReturn("activity2").thenReturn("activity3");
        DBManager dbManager = DBManager.getInstance();
        List<Activity> activities = dbManager.getCompletedActivities(con, 1);
        Assert.assertEquals(3, activities.size());
    }

    @Test
    public void testGetCustomCategories() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.GET_CUSTOM_CATEGORIES);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(rs.getString(FieldNames.CUSTOM_CATEGORY_NAME)).thenReturn("activity1").thenReturn("activity2").thenReturn("activity3");
        DBManager dbManager = DBManager.getInstance();
        List<CustomCategory> customCategories = dbManager.getCustomCategories(con);
        Assert.assertEquals(3, customCategories.size());
    }

    @Test
    public void testGetActivitiesByCategory() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.FIND_ACTIVITIES_BY_CATEGORY);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(rs.getString(FieldNames.ACTIVITY_NAME)).thenReturn("activity1").thenReturn("activity2").thenReturn("activity3");
        DBManager dbManager = DBManager.getInstance();
        List<Activity> activities = dbManager.getActivitiesByCategory(con, "Requested", 1);
        Assert.assertEquals(3, activities.size());
    }

    @Test
    public void testGetRequestedActivities() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.FIND_REQUESTED_ACTIVITIES);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(rs.getString(FieldNames.ACTIVITY_NAME)).thenReturn("activity1").thenReturn("activity2").thenReturn("activity3");
        DBManager dbManager = DBManager.getInstance();
        List<Activity> activities = dbManager.getRequestedActivities(con, 1);
        Assert.assertEquals(3, activities.size());
    }

    @Test
    public void testGetAllRequestedActivities() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.FIND_ALL_REQUESTED_ACTIVITIES);
        Mockito.when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(rs.getString(FieldNames.ACTIVITY_NAME)).thenReturn("activity1").thenReturn("activity2").thenReturn("activity3");
        DBManager dbManager = DBManager.getInstance();
        List<Activity> activities = dbManager.getAllRequestedActivities(con);
        Assert.assertEquals(3, activities.size());
    }

    @Test
    public void getCustomCategoryId() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.GET_CUSTOM_CATEGORY_ID);
        Mockito.when(rs.next()).thenReturn(true);
        Mockito.when(rs.getInt(FieldNames.CUSTOM_CATEGORY_ID)).thenReturn(1);
        DBManager dbManager = DBManager.getInstance();
        int customCategoryId = dbManager.getCustomCategoryId(con, "category");
        Assert.assertEquals(1, customCategoryId);
    }

    @Test
    public void testCompleteActivity() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.COMPLETE_ACTIVITY);
        Mockito.when(pstmt.executeUpdate()).thenReturn(1);
        DBManager dbManager = DBManager.getInstance();
        int updatedRows = dbManager.completeActivity(con, "1", "");
        Assert.assertEquals(1, updatedRows);
    }

    @Test
    public void testRegisterUser() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.REGISTER_USER);
        Mockito.when(pstmt.execute()).thenReturn(true);
        DBManager dbManager = DBManager.getInstance();
        boolean updatedRows = dbManager.registerUser(con, new User());
        Assert.assertTrue(updatedRows);
    }

    @Test
    public void testConfirmActivity() throws SQLException {
        initStatement(PreparedStatement.class, SQLCommands.CONFIRM_ACTIVITY);
        Mockito.when(pstmt.executeUpdate()).thenReturn(1);
        DBManager dbManager = DBManager.getInstance();
        int updatedRows = dbManager.confirmActivity(con, "1");
        Assert.assertEquals(1, updatedRows);
    }

    @Before
    public void setUp() {
        con = Mockito.mock(Connection.class);
        stmt = Mockito.mock(Statement.class);
        pstmt = Mockito.mock(PreparedStatement.class);
        rs = Mockito.mock(ResultSet.class);

        dbUtils = Mockito.mockStatic(DBUtils.class);
        dbUtils.when(DBUtils::getConnection).thenReturn(con);
    }

    @After
    public void tearDown() {
        dbUtils.close();
    }

    private void initStatement(Class<? extends Statement> stmtClass, String query) throws SQLException {
        if (stmtClass == Statement.class) {
            Mockito.when(stmt.executeQuery(query))
                    .thenReturn(rs);
            Mockito.when(con.createStatement()).thenReturn(stmt);
        }
        if (stmtClass == PreparedStatement.class) {
            Mockito.when(pstmt.executeQuery()).thenReturn(rs);
            Mockito.when(con.prepareStatement(query)).thenReturn(pstmt);
        }
    }
}
