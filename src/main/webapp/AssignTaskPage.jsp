<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale" value="${not empty sessionScope.locale ? sessionScope.locale : 'en'}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources"/>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bs5/css/bootstrap.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-4">
            <div class="assign-form bg-light mt-4 p-4">
                <form action="${pageContext.request.contextPath}/controller" method="post">
                    <input type="hidden" name="command" value="assignActivity">
                    <div class="mb-auto">
                        <label for="validationDefault01" class="form-label"><fmt:message key="assigntask.taskdescription"/></label>
                        <input type="text" class="form-control" name="activityName" id="validationDefault01"
                               placeholder="<fmt:message key="assigntask.taskdescription"/>" required>
                    </div>
                    <div class="mb-auto">
                        <label for="activityEnd" class="form-label"><fmt:message key="assigntask.deadline"/></label>
                        <input type="datetime-local" name="activityEnd" id="activityEnd" min="${requestScope.currentDate}"
                               required>
                    </div>
                    <div class="mb-auto">
                        <label for="selectedUser" class="form-label"><fmt:message key="assigntask.users"/></label>
                        <select class="form-select" id="selectedUser" name="executorsLogins" multiple>
                            <c:forEach var="user" items="${requestScope.allUsers}">
                                <option>
                                        ${user.userLogin}
                                </option>
                            </c:forEach>
                        </select>
                        <select class="form-select" id="selectedCustomCategory" name="selectedCustomCategory">
                            <c:forEach var="category" items="${requestScope.customCategories}">
                                <option>
                                        ${category.customCategoryName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="importantTaskCheck" value=""
                                   id="invalidCheck2">
                            <label class="form-check-label" for="invalidCheck2">
                                <fmt:message key="assigntask.mark"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-primary" type="submit"><fmt:message key="assigntask.submit"/></button>
                    </div>
                </form>
            </div>
        </div>
        <form action="${pageContext.request.contextPath}/MainPage.jsp">
            <button class="btn btn-primary" type="submit"><fmt:message key="assigntask.cancel"/></button>
        </form>
    </div>
</div>

<script src="https://unpkg.com/@popperjs/core@2.4.0/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bs5/js/bootstrap.js"></script>
</body>
</html>
