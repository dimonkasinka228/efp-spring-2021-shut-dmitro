package com.my.Commands;

import com.my.DB.ActivityManager;
import com.my.DB.CustomCategoryManager;
import com.my.DB.Entity.Activity;
import com.my.DB.Entity.User;
import com.my.DB.UserManager;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.Level.*;

/**
 * Implements functionality of assigning activity with user or list of users.
 * All the information coming from request(List of user and activity
 * description
 */
public class AssignActivityCommand implements Command {
    private static final Logger log = LogManager.getLogger(AssignActivityCommand.class);
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String customCommand = req.getParameter("selectedCustomCategory");
        log.log(DEBUG, "Custom command name is " + customCommand);
        String taskDescription = req.getParameter("activityName");
        log.log(DEBUG, "Description of task is " + taskDescription);
        String taskEnd = req.getParameter("activityEnd");
        log.log(DEBUG, "End time of task is " + taskEnd);
        String[] usersLogins = req.getParameterValues("executorsLogins");
        String importantCheck = req.getParameter("importantTaskCheck");
        //when is not selected then return null, else return empty String
        UserManager userManager = UserManager.getInstance();
        List<User> executors = new ArrayList<>();
        for (int i = 0; i < usersLogins.length; i++) {
            User user = userManager.getUser(usersLogins[i]);
            executors.add(user);
        }

        if (executors.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "There must be at least one user");
            req.setAttribute("returnLink", JSPNames.ASSIGN_ACTIVITY_PAGE_JSP);
            return address;
        }
        if (taskDescription.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "Task description must not be empty");
            req.setAttribute("returnLink", JSPNames.ASSIGN_ACTIVITY_PAGE_JSP);
            return address;
        }
        boolean isImportant = false;
        if (importantCheck != null) {
            isImportant = true;
        }
        CustomCategoryManager customCategoryManager = CustomCategoryManager.getInstance();
        int customCategoryId = customCategoryManager.getCustomCategoryId(customCommand);
        Activity activity = new Activity();
        activity.setCustomCategoryName(customCommand);
        activity.setActivityName(taskDescription);
        activity.setActivityDeadline(taskEnd);
        activity.setCustomCategoryId(customCategoryId);
        activity.setCustomCategoryName(customCommand);

        ActivityManager activityManager = ActivityManager.getInstance();
        activityManager.assignActivity(activity, executors, isImportant);
        log.log(DEBUG, "Assigned activity");
        address = JSPNames.MAIN_PAGE_JSP;
        return address;
    }
}
