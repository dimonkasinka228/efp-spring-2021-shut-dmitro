package com.my.Commands;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implements functionality of leaving account.
 * Clears data about previous user in session
 * and opens page with logging
 */
public class LeaveAccountCommand implements Command {
    Logger log = LogManager.getLogger(LeaveAccountCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.LOGIN_PAGE_JSP;
        if (req.getSession().getAttribute("loggedUser") == null) {
            return address;
        }
        req.getSession().removeAttribute("loggedUser");
        req.getSession().removeAttribute("taskList");
        log.log(Level.DEBUG, "Cleared session and left account");
        return address;
    }
}
