package com.my.Commands;

import com.my.DB.ActivityManager;
import com.my.DB.Entity.Activity;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CompleteTaskCommand implements Command {
    /**
     * Command that implements functionality of completing activity.
     * Writes time, when user marked activity as completed, to database.
     * If activity is uncompleted then ending time is NULL
     */
    private static final Logger log = LogManager.getLogger(CompleteTaskCommand.class);
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.MAIN_PAGE_JSP;
        String activityId = req.getParameter("endedTaskId");
        log.log(Level.DEBUG, "Id of ended task " + activityId);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        ActivityManager activityManager = ActivityManager.getInstance();
        activityManager.completeActivity(activityId, currentDate);
        ArrayList<Activity> activities = (ArrayList<Activity>)req.getSession().getAttribute("taskList");
        activities.removeIf(activity -> String.valueOf(activity.getActivityId()).equals(activityId));
        log.log(Level.DEBUG, "Completed task " + activityId);
        return address;
    }
}
