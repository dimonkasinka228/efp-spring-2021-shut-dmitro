package com.my.Commands;

import com.my.DB.Entity.User;
import com.my.DB.UserManager;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implements functionality of registration user in DB.
 * Checks if user with this login exist in DB, checks if fields is empty and
 * if everything alright then creates new user in DB
 */
public class RegisterCommand implements Command {
    private static final Logger log = LogManager.getLogger(RegisterCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String username = req.getParameter("username");
        String login = req.getParameter("login");
        String lastname = req.getParameter("lastname");
        String password = req.getParameter("password");
        if (username == null || username.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "Username is empty");
            req.getSession().setAttribute("returnLink", JSPNames.REGISTER_PAGE_JSP);
            return address;
        }
        if (lastname == null || lastname.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "Lastname is empty");
            req.getSession().setAttribute("returnLink", JSPNames.REGISTER_PAGE_JSP);
            return address;
        }
        if (password == null || password.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "Password is empty");
            req.getSession().setAttribute("returnLink", JSPNames.REGISTER_PAGE_JSP);
            return address;
        }
        if (login == null || login.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "Login is empty");
            req.getSession().setAttribute("returnLink", JSPNames.REGISTER_PAGE_JSP);
            return address;
        }

        User user = new User();
        user.setUserLogin(login);
        user.setUserName(username);
        user.setUserSurname(lastname);
        user.setUserPassword(password);
        user.setRoleId(2);
        log.log(Level.DEBUG, "Registered user is " + user);
        UserManager userManager = UserManager.getInstance();
        User tempUser = userManager.getUser(login);
        log.log(Level.DEBUG, "Checked user for login duplication is " + tempUser);
        if (tempUser != null) {
            req.getSession().setAttribute("errorMessage", "User with this login already exists");
            req.getSession().setAttribute("returnLink", JSPNames.REGISTER_PAGE_JSP);
            return address;
        }

        address = JSPNames.LOGIN_PAGE_JSP;
        userManager.registerUser(user);
        return address;
    }
}
