package com.my.Commands;

import com.my.DB.CustomCategoryManager;
import com.my.DB.Entity.CustomCategory;
import com.my.DB.Entity.User;
import com.my.Util.DBException;
import com.my.DB.UserManager;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Implements functionality of logging user
 * with validation field if it's empty and check if
 * user with input login is exists in Database and check
 * if password is correct
 */
public class LoginCommand implements Command {
    private static final Logger log = LogManager.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String login = req.getParameter("login");
        log.log(Level.DEBUG, "Logged user login: " + login);
        String password = req.getParameter("password");
        log.log(Level.DEBUG, "Logged user password: " + password);
        req.getSession().removeAttribute("loggedUser");
        req.getSession().removeAttribute("taskList");
        UserManager userManager = UserManager.getInstance();
        User user = userManager.getUser(login);
        if (user == null) {
            req.getSession().setAttribute("errorMessage", "This user do not exist!");
            log.log(Level.ERROR, "User " + login + " does not exist");
            req.getSession().setAttribute("returnLink", JSPNames.LOGIN_PAGE_JSP);
            return address;
        }
        if (!user.getUserPassword().equals(password)) {
            req.getSession().setAttribute("errorMessage", "Wrong user password!");
            log.log(Level.ERROR, "Wrong password for user " + login);
            req.getSession().setAttribute("returnLink", JSPNames.LOGIN_PAGE_JSP);
            return address;
        }

        log.log(Level.DEBUG, "User " + login + " is logged");
        address = JSPNames.MAIN_PAGE_JSP;
        req.getSession().setAttribute("loggedUser", user);
        CustomCategoryManager customCategoryManager = CustomCategoryManager.getInstance();
        List<CustomCategory> customCategories = customCategoryManager.getCustomCategories();
        req.getSession().setAttribute("customCategories", customCategories);
        log.log(Level.DEBUG, "User " + login + " is put to session");
        log.log(Level.DEBUG, "Logged user is " + user);

        return address;
    }
}
