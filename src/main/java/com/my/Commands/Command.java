package com.my.Commands;

import com.my.Util.DBException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {

    String execute(HttpServletRequest req,
                   HttpServletResponse response) throws DBException;
}
