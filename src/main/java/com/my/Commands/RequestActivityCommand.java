package com.my.Commands;

import com.my.DB.ActivityManager;
import com.my.DB.Entity.Activity;
import com.my.DB.Entity.User;
import com.my.Util.DBException;
import com.my.Util.JSPNames;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implements functionality of requesting activity by user,
 * all information comes from request
 */
public class RequestActivityCommand implements Command{
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String taskDescription = req.getParameter("activityName");
        String taskEnd = req.getParameter("activityEnd");
        User user = (User) req.getSession().getAttribute("loggedUser");

        if (user == null) {
            req.getSession().setAttribute("errorMessage", "You are not logged and cant request new activity!");
            req.getSession().setAttribute("returnLink", JSPNames.LOGIN_PAGE_JSP);
            return address;
        }
        if (taskDescription == null) {
            req.getSession().setAttribute("errorMessage", "Task description can not be empty!");
            req.getSession().setAttribute("returnLink", JSPNames.TASK_REQUESTS_PAGE_JSP);
            return address;
        }
        if (taskEnd == null) {
            req.getSession().setAttribute("errorMessage", "Task deadline can not be empty!");
            req.getSession().setAttribute("returnLink", JSPNames.TASK_REQUESTS_PAGE_JSP);
            return address;
        }

        Activity activity = new Activity();
        activity.setActivityName(taskDescription);
        activity.setActivityDeadline(taskEnd);
        activity.setCustomCategoryId(1);
        ActivityManager activityManager = ActivityManager.getInstance();
        activityManager.requestTask(activity, user);
        address = JSPNames.MAIN_PAGE_JSP;
        return address;
    }
}
