package com.my.Commands;

import com.my.DB.Entity.Activity;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Command that implements filtrating activities by exciting
 * categories. Removes activities from list in session if
 * it's category in not equal to filtrated
 */
public class CategoryFilterCommand implements Command {
    Logger log = LogManager.getLogger(CategoryFilterCommand.class);
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String categoryFilter = req.getParameter("categoryFilter");
        List<Activity> activities = (List<Activity>) req.getSession().getAttribute("taskList");
        if (categoryFilter.isEmpty()) {
            req.getSession().setAttribute("errorMessage", "Category can not be empty");
            req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
            log.log(Level.ERROR, "Empty category");
            return address;
        }
        if (activities == null) {
            req.getSession().setAttribute("errorMessage", "Nothing to be filtrated");
            req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
            log.log(Level.ERROR, "List of activities is empty");
            return address;
        }
        address = JSPNames.MAIN_PAGE_JSP;
        activities = activities.stream().filter(activity -> activity.getCustomCategoryName().equalsIgnoreCase(categoryFilter)).collect(Collectors.toList());
        req.getSession().setAttribute("taskList", activities);
        log.log(Level.DEBUG, "Assigned list if tasks");
        return address;
    }
}
