package com.my.Commands;

import com.my.DB.ActivityManager;
import com.my.DB.Entity.Activity;
import com.my.DB.Entity.User;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.apache.logging.log4j.Level.*;

/**
 * Command that getting list of necessary commands by its type
 * that coming from request
 */
public class TaskTypeCommand implements Command {
    private static final Logger log = LogManager.getLogger(TaskTypeCommand.class);

    /**
     * @param req
     * @param response
     * @return string address
     * @throws DBException
     */
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String taskType = req.getParameter("taskType");
        ActivityManager activityManager = ActivityManager.getInstance();
        List<Activity> activities;
        User currentUser = (User) req.getSession().getAttribute("loggedUser");

        if (currentUser == null) {
            req.getSession().setAttribute("errorMessage", "You are not logged");
            log.log(ERROR, "User not logged, cannot get tasks list");
            return address;
        }
        log.log(DEBUG, "Type of task is " + taskType);
        log.log(DEBUG, "Current user is " + currentUser);
        address = JSPNames.MAIN_PAGE_JSP;
        req.getSession().setAttribute("taskType", taskType);
        //open completed tasks
        if (taskType.equalsIgnoreCase("Completed")) {
            activities = activityManager.getCompletedActivities(currentUser.getUserId());
            log.log(DEBUG, "Got completed task for user " + currentUser.getUserLogin());
            req.getSession().setAttribute("taskList", activities);
            return address;
        }
        if (taskType.equalsIgnoreCase("Requested")) {
            //if role id = 2 then it is common user and we must get only user's requests
            if (currentUser.getRoleId() == 2) {
                log.log(DEBUG, "Getting requested for user");
                activities = activityManager.getRequestedActivities(currentUser.getUserId());
                req.getSession().setAttribute("taskList", activities);
            }
            //if role id = 1 then it is admin and we must get all requests
            if (currentUser.getRoleId() == 1) {
                log.log(DEBUG, "Getting requested for admin");
                activities = activityManager.getAllRequestedActivities();
                req.getSession().setAttribute("taskList", activities);
            }
            return address;
        }
        //in another case we getting activities with task type
        activities = activityManager.getActivitiesByCategory(taskType, currentUser.getUserId());
        log.log(DEBUG, "Got " + taskType + " activities for user " + currentUser.getUserLogin());
        req.getSession().setAttribute("taskList", activities);
        log.log(DEBUG, "Set task list for user " + currentUser.getUserLogin());
        return address;
    }
}

