package com.my.Commands;

import com.my.DB.CustomCategoryManager;
import com.my.DB.Entity.CustomCategory;
import com.my.DB.Entity.User;
import com.my.DB.UserManager;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Used for preparing information before assigning task for users.
 * Gets categories from DB, gets minimal time to prevent inputting
 * time in past and list of all users, who can get activity
 */
public class PrepareAssigningInfo implements Command {
    private static final Logger log = LogManager.getLogger(PrepareAssigningInfo.class);
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        List<User> users;
        UserManager userManager = UserManager.getInstance();
        CustomCategoryManager customCategoryManager = CustomCategoryManager.getInstance();
        List<CustomCategory> customCategories = customCategoryManager.getCustomCategories();
        users = userManager.getAllUsers();
        log.log(Level.DEBUG, "Got list of all users");
        if (users.isEmpty()) {
            log.log(Level.ERROR, "List of users is empty");
            return address;
        }
        address = JSPNames.ASSIGN_ACTIVITY_PAGE_JSP;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm");
        Date date = new Date();
        req.setAttribute("currentDate", (dateFormat.format(date)) + "T" + (timeFormat.format(date)));
        req.setAttribute("customCategories", customCategories);
        req.setAttribute("allUsers", users);
        return address;
    }
}
