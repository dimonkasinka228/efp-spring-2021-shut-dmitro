package com.my.Commands;

import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command that changes locale in session.
 * Required locale comes from request
 */
public class ChangeLocaleCommand implements Command {
    Logger log = LogManager.getLogger(ChangeLocaleCommand.class);
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.MAIN_PAGE_JSP;
        String locale = req.getParameter("locale");
        req.getSession().setAttribute("locale", locale);
        log.log(Level.DEBUG, "Changed locale to " + locale);
        return address;
    }
}
