package com.my.Commands;

import com.my.Util.DBException;
import com.my.Util.JSPNames;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Used for preparing information before requesting task.
 * Gets minimal time to prevent inputting
 * time in past.
 */
public class PrepareRequestingInfoCommand implements Command {
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.TASK_REQUESTS_PAGE_JSP;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm");
        Date date = new Date();
        req.setAttribute("currentDate", (dateFormat.format(date)) + "T" + (timeFormat.format(date)));
        return address;
    }
}
