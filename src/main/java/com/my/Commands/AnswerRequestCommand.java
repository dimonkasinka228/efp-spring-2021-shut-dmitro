package com.my.Commands;

import com.my.DB.ActivityManager;
import com.my.DB.Entity.Activity;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

public class AnswerRequestCommand implements Command{
    /**
     * Command that implements functionality
     * of controlling activities(confirm, reject, cancel request)
     */
    private static final Logger log = LogManager.getLogger(AnswerRequestCommand.class);
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse response) throws DBException {
        String address = JSPNames.ERROR_JSP;
        String answer = req.getParameter("answer");
        log.log(Level.DEBUG, "Answer to request is " + answer);
        String activityId = req.getParameter("endedTaskId");
        log.log(Level.DEBUG, "Answered activity id is " + activityId);
        ActivityManager activityManager = ActivityManager.getInstance();

        if (activityId == null) {
            req.getSession().setAttribute("errorMessage", "Wrong activity parameters");
            req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
            log.log(Level.ERROR, "Activity id is null, cannot answer on id");
            return address;
        }

        if (answer == null) {
            req.getSession().setAttribute("errorMessage", "Wrong answer parameter");
            req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
            log.log(Level.ERROR, "Answer to request is null");
            return address;
        }
        address = JSPNames.MAIN_PAGE_JSP;
        if (answer.equalsIgnoreCase("confirm")) {
            activityManager.confirmActivity(activityId);
            log.log(Level.DEBUG, "Confirmed request successfully");
        }

        if (answer.equalsIgnoreCase("reject")) {
            activityManager.rejectActivity(activityId);
            log.log(Level.DEBUG, "Rejected request successfully");
        }
        ArrayList<Activity> activities = (ArrayList<Activity>)req.getSession().getAttribute("taskList");
        activities.removeIf(activity -> String.valueOf(activity.getActivityId()).equals(activityId));
        log.log(Level.DEBUG, "Completed task " + activityId);
        return address;
    }
}
