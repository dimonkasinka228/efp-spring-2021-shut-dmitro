package com.my.DB.Entity;

public class CustomCategory {
    private int customCategoryId;
    private String customCategoryName;
    private int userId;

    public int getCustomCategoryId() {
        return customCategoryId;
    }

    public void setCustomCategoryId(int customCategoryId) {
        this.customCategoryId = customCategoryId;
    }

    public String getCustomCategoryName() {
        return customCategoryName;
    }

    public void setCustomCategoryName(String customCategoryName) {
        this.customCategoryName = customCategoryName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
