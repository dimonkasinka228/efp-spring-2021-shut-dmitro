package com.my.DB.Entity;

import java.io.Serializable;

public class Role implements Serializable {
    private static final long serialVersionUID = 9059743970057284249L;

    private int roleIf;
    private String roleName;

    public int getRoleIf() {
        return roleIf;
    }

    public void setRoleIf(int roleIf) {
        this.roleIf = roleIf;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
