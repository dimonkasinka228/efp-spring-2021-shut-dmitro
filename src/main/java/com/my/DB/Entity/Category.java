package com.my.DB.Entity;

import java.io.Serializable;

public class Category implements Serializable {
    private static final long serialVersionUID = -8032008874759925964L;
    private int categoryId;
    private String categoryName;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
