package com.my.DB.Entity;

import java.io.Serializable;

public class Activity implements Serializable {

    private static final long serialVersionUID = -6024234198824156962L;

    private int activityId;
    private String activityName;
    private String activityDeadline;
    private int activityCategoryId;
    private String activityEndTime;
    private int customCategoryId;
    private String customCategoryName;
    private int usersAmount;

    public int getUsersAmount() {
        return usersAmount;
    }

    public void setUsersAmount(int usersAmount) {
        this.usersAmount = usersAmount;
    }

    public String getCustomCategoryName() {
        return customCategoryName;
    }

    public void setCustomCategoryName(String customCategoryName) {
        this.customCategoryName = customCategoryName;
    }

    public int getCustomCategoryId() {
        return customCategoryId;
    }

    public void setCustomCategoryId(int customCategoryId) {
        this.customCategoryId = customCategoryId;
    }


    public String getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityDeadline() {
        return activityDeadline;
    }

    public void setActivityDeadline(String activityDeadline) {
        this.activityDeadline = activityDeadline;
    }

    public int getActivityCategoryId() {
        return activityCategoryId;
    }

    public void setActivityCategoryId(int activityCategoryId) {
        this.activityCategoryId = activityCategoryId;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activityId=" + activityId +
                ", activityName='" + activityName + '\'' +
                ", activityDeadline='" + activityDeadline + '\'' +
                ", activityCategoryId=" + activityCategoryId +
                '}' +System.lineSeparator();
    }
}
