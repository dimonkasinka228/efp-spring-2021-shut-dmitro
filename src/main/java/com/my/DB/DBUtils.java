package com.my.DB;

import org.apache.logging.log4j.Level;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DBUtils {
    private static DataSource ds;

    public static void init() {
        if (ds != null) {
            throw new IllegalStateException("Cannot initialize DataSource twice");
        }
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/time_organisation");
        } catch (NamingException ex) {
            throw new IllegalStateException("Cannot init DBManager");
        }

    }

    public static Connection getConnection() {
        Connection con = null;
        try {
            con = ds.getConnection();
        } catch (SQLException ex) {
            throw new IllegalStateException("Cannot obtain a connection", ex);
        }
        return con;
    }
}
