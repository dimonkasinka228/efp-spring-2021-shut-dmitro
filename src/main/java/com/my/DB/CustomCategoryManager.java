package com.my.DB;

import com.my.DB.Entity.CustomCategory;
import com.my.Util.DBException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CustomCategoryManager {

    private static CustomCategoryManager instance;
    private static final Logger log = LogManager.getLogger(CustomCategoryManager.class);

    public static synchronized CustomCategoryManager getInstance() {
        if (instance == null) {
            instance = new CustomCategoryManager();
            log.log(Level.DEBUG, "Got Activity Manager instance");
        }
        return instance;
    }

    private CustomCategoryManager() {
    }

    /**
     * Gets id of category with name in parameter
     * @param customCategoryName
     * @return
     * @throws DBException
     */
    public int getCustomCategoryId(String customCategoryName) throws DBException {
        int customCategoryId;
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            customCategoryId = dbManager.getCustomCategoryId(connection, customCategoryName);
            log.log(Level.DEBUG, "Got custom categories id with name " + customCategoryName);
            connection.commit();
            log.log(Level.DEBUG, "Committed getting custom category id with name " + customCategoryName);
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot get custom category id with name " + customCategoryName);
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback getting custom categories id with name " + customCategoryName);
            throw new DBException("Cannot get custom category id", e);
        } finally {
            dbManager.close(connection);
        }
        return customCategoryId;

    }

    /**
     * Return all categories from DB
     * @return
     * @throws DBException
     */
    public List<CustomCategory> getCustomCategories() throws DBException {
        List<CustomCategory> customCategories;
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            customCategories = dbManager.getCustomCategories(connection);
            log.log(Level.DEBUG, "Got custom categories for user with ");
            connection.commit();
            log.log(Level.DEBUG, "Committed getting custom categories");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot get custom categories");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback getting custom categories");
            throw new DBException("Cannot get custom categories", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
        return customCategories;
    }
}
