package com.my.DB;

import com.my.DB.Entity.Activity;
import com.my.DB.Entity.CustomCategory;
import com.my.DB.Entity.User;
import com.my.Util.FieldNames;
import com.my.Util.SQLCommands;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DBManager {
    private static DBManager instance;
    private static final Logger log = LogManager.getLogger(DBManager.class);
    //tested
    public User getUser(Connection con, String login) throws SQLException {
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.FIND_USER_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            log.log(Level.DEBUG, "Executed searching user " + login + " by login");
            if (rs.next()) {
                user = extractUser(rs);
                log.log(Level.DEBUG, "Got user with login " + login);
            }
        } finally {
            close(rs);
            close(pstmt);
        }
        return user;
    }
    //tested
    public List<User> getAllUsers(Connection con) throws SQLException {
        List<User> users = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.GET_ALL_USERS);
            rs = pstmt.executeQuery();
            log.log(Level.DEBUG, "Executed getting all users");
            while (rs.next()) {
                User user = extractUser(rs);
                users.add(user);
            }
            log.log(Level.DEBUG, "Collected list of users");
        } finally {
            close(rs);
            close(pstmt);
        }
        return users;
    }
    //tested
    public List<Activity> getCompletedActivities(Connection con, int userId) throws SQLException {
        List<Activity> activities = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.FIND_COMPLETED_ACTIVITIES);
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();
            log.log(Level.DEBUG, "Executed searching completed activities for user " + userId);
            while (rs.next()) {
                Activity activity = extractActivity(rs);
                activities.add(activity);
            }
        } finally {
            close(rs);
            close(pstmt);
        }
        log.log(Level.DEBUG, "Collected list of completed activities for user " + userId);
        return activities;
    }
    //tested
    public List<CustomCategory> getCustomCategories(Connection con) throws SQLException {
        List<CustomCategory> customCategories = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.GET_CUSTOM_CATEGORIES);
            rs = pstmt.executeQuery();
            log.log(Level.DEBUG, "Got custom categories");
            while (rs.next()) {
                customCategories.add(extractCustomCategory(rs));
            }
        } finally {
            close(rs);
            close(pstmt);
        }
        return customCategories;
    }

    //tested
    public List<Activity> getActivitiesByCategory(Connection con, String categoryName, int userId) throws SQLException {
        List<Activity> activities = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.FIND_ACTIVITIES_BY_CATEGORY);
            pstmt.setInt(1, userId);
            pstmt.setString(2, categoryName);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activities.add(extractActivity(rs));
            }
        } finally {
            close(rs);
            close(pstmt);
        }
        return activities;
    }

    //tested
    public List<Activity> getAllRequestedActivities(Connection con) throws SQLException {
        PreparedStatement pstmt = null;
        List<Activity> activities = new ArrayList<>();
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.FIND_ALL_REQUESTED_ACTIVITIES);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activities.add(extractActivity(rs));
            }
        } finally {
            close(rs);
            close(pstmt);
        }
        return activities;
    }
    //tested
    public List<Activity> getRequestedActivities(Connection con, int userId) throws SQLException {
        PreparedStatement pstmt = null;
        List<Activity> activities = new ArrayList<>();
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.FIND_REQUESTED_ACTIVITIES);
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activities.add(extractActivity(rs));
            }
        } finally {
            close(rs);
            close(pstmt);
        }
        return activities;
    }
    //tested
    public int getCustomCategoryId(Connection con, String customCategoryName) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.GET_CUSTOM_CATEGORY_ID);
            pstmt.setString(1, customCategoryName);
            rs = pstmt.executeQuery();
            rs.next();
            return rs.getInt(FieldNames.CUSTOM_CATEGORY_ID);
        } finally {
            close(pstmt);
            close(rs);
        }
    }
    //tested
    public int completeActivity(Connection con, String activityId, String currentDate) throws SQLException {
        PreparedStatement pstmt = null;
        int updatedRows;
        try {
            pstmt = con.prepareStatement(SQLCommands.COMPLETE_ACTIVITY);
            pstmt.setString(1, currentDate);
            pstmt.setString(2, activityId);
            updatedRows = pstmt.executeUpdate();
        } finally {
            close(pstmt);
        }
        return updatedRows;
    }

    public synchronized boolean assignActivity(Connection con, Activity activity, List<User> users, boolean isImportant) throws SQLException {
        PreparedStatement pstmt = null;
        con.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
        log.log(Level.DEBUG, "Got connection " + con);
        boolean insertedRows;
        try {
            pstmt = con.prepareStatement(SQLCommands.INSERT_ACTIVITY);
            pstmt.setString(1, activity.getActivityName());
            pstmt.setString(2, activity.getActivityDeadline());
            pstmt.setString(3, "1");
            pstmt.setInt(4, activity.getCustomCategoryId());
            log.log(Level.DEBUG, "Set activity name is " + activity.getActivityName());
            log.log(Level.DEBUG, "Set activity deadline is " + activity.getActivityDeadline());
            log.log(Level.DEBUG, "Set activity category is " + activity.getActivityCategoryId());
            if (isImportant) {
                pstmt.setString(3, "2");
            }
            insertedRows = pstmt.execute();
            int latestActivityId = getLatestActivityId(con);
            for (User user : users) {
                getInstance().assignActivityWithUser(con, latestActivityId, user.getUserId());
            }
        } finally {
            close(pstmt);
        }
        return insertedRows;
    }

    public synchronized void requestActivity(Connection con, Activity activity, User user) throws SQLException {
        PreparedStatement pstmt = null;
        con.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
        try {
            pstmt = con.prepareStatement(SQLCommands.REQUEST_ACTIVITY);
            pstmt.setString(1, activity.getActivityName());
            pstmt.setString(2, activity.getActivityDeadline());
            pstmt.setString(3, "4");
            pstmt.setInt(4, activity.getCustomCategoryId());
            pstmt.execute();
            int latestActivityId = getInstance().getLatestActivityId(con);
            getInstance().assignActivityWithUser(con, latestActivityId, user.getUserId());
        } finally {
            close(pstmt);
        }
    }
    //tested
    public int confirmActivity(Connection con, String activityId) throws SQLException {
        PreparedStatement pstmt = null;
        int updatedRows;
        try {
            pstmt = con.prepareStatement(SQLCommands.CONFIRM_ACTIVITY);
            pstmt.setString(1, activityId);
            updatedRows = pstmt.executeUpdate();
        } finally {
            close(pstmt);
        }
        return updatedRows;
    }

    public int rejectActivity(Connection con, String activityId) throws SQLException {
        PreparedStatement pstmt = null;
        int updatedRows;
        try {
            removeAllAssignments(con, activityId);
            pstmt = con.prepareStatement(SQLCommands.REJECT_ACTIVITY);
            pstmt.setString(1, activityId);
            updatedRows = pstmt.executeUpdate();
        } finally {
            close(pstmt);
        }
        return updatedRows;
    }
    //tested
    public boolean registerUser(Connection con, User user) throws SQLException {
        PreparedStatement pstmt = null;
        boolean insertedRow;
        try {
            pstmt = con.prepareStatement(SQLCommands.REGISTER_USER);
            pstmt.setString(1, user.getUserName());
            pstmt.setString(2, user.getUserSurname());
            pstmt.setString(3, user.getUserPassword());
            pstmt.setString(4, user.getUserLogin());
            pstmt.setInt(5, user.getRoleId());
            insertedRow = pstmt.execute();
        } finally {
            close(pstmt);
        }
        return insertedRow;
    }

    private void removeAllAssignments(Connection con, String activityId) throws SQLException {
        PreparedStatement pstmt;
        pstmt = con.prepareStatement(SQLCommands.REMOVE_ASSIGNMENTS);
        pstmt.setString(1, activityId);
        pstmt.execute();
    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
            log.log(Level.TRACE, "Created DBManager instance");
        }
        return instance;
    }

    public void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void rollback(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private Activity extractActivity(ResultSet rs) throws SQLException {
        Activity activity = new Activity();
        activity.setActivityId(rs.getInt(FieldNames.ACTIVITY_ID));
        activity.setActivityName(rs.getString(FieldNames.ACTIVITY_NAME));
        activity.setActivityDeadline(rs.getString(FieldNames.ACTIVITY_DEADLINE));
        activity.setActivityCategoryId(rs.getInt(FieldNames.ACTIVITY_CATEGORY_ID));
        activity.setActivityEndTime(rs.getString(FieldNames.ACTIVITY_END_TIME));
        activity.setCustomCategoryId(rs.getInt(FieldNames.CUSTOM_CATEGORY_ID));
        activity.setCustomCategoryName(rs.getString(FieldNames.CUSTOM_CATEGORY_NAME));
        return activity;
    }

    private CustomCategory extractCustomCategory(ResultSet rs) throws SQLException {
        CustomCategory customCategory = new CustomCategory();
        customCategory.setCustomCategoryId(rs.getInt(FieldNames.CUSTOM_CATEGORY_ID));
        customCategory.setCustomCategoryName(rs.getString(FieldNames.CUSTOM_CATEGORY_NAME));
        customCategory.setUserId(rs.getInt(FieldNames.CUSTOM_CATEGORY_USER_ID));
        return customCategory;
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setUserId(rs.getInt(FieldNames.USER_ID));
        user.setUserLogin(rs.getString(FieldNames.USER_LOGIN));
        user.setUserName(rs.getString(FieldNames.USER_NAME));
        user.setUserSurname(rs.getString(FieldNames.USER_SURNAME));
        user.setUserPassword(rs.getString(FieldNames.USER_PASSWORD));
        user.setRoleId(rs.getInt(FieldNames.USER_ROLE_ID));
        return user;
    }

    private int getLatestActivityId(Connection con) throws SQLException {
        int latestActivityId;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.GET_LATEST_ACTIVITY);
            rs = pstmt.executeQuery();
            rs.next();
            latestActivityId = rs.getInt("activity_id");
        } finally {
            close(rs);
            close(pstmt);
        }
        return latestActivityId;
    }

    private void assignActivityWithUser(Connection con, int activityId, int userId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(SQLCommands.ASSIGN_ACTIVITY_WITH_USER);
            pstmt.setInt(1, userId);
            pstmt.setInt(2, activityId);
            pstmt.execute();
        } finally {
            close(pstmt);
        }
    }
}