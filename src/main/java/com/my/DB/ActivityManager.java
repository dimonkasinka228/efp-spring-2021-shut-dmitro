package com.my.DB;

import com.my.DB.Entity.Activity;
import com.my.DB.Entity.User;
import com.my.Util.DBException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ActivityManager {

    private static ActivityManager instance;
    private static final Logger log = LogManager.getLogger(ActivityManager.class);

    public static synchronized ActivityManager getInstance() {
        if (instance == null) {
            instance = new ActivityManager();
            log.log(Level.DEBUG, "Got Activity Manager instance");
        }
        return instance;
    }

    private ActivityManager() {
    }

    /**
     * Getting completed activities for user
     * with id in parameter
     * @param userId
     * @return
     * @throws DBException
     */
    public List<Activity> getCompletedActivities(int userId) throws DBException {
        List<Activity> activities;
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            activities = dbManager.getCompletedActivities(connection, userId);
            log.log(Level.DEBUG, "Got completed task for user with " + userId + " id");
            connection.commit();
            log.log(Level.DEBUG, "Committed getting completed tasks for user with " + userId + " id");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot get completed task for user with " + userId + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback getting completed tasks for user with " + userId + " id");
            throw new DBException("Cannot get completed activities", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
        return activities;
    }

    /**
     * Get activities with required category for user with id
     * from parameter
     * @param categoryName
     * @param userId
     * @return
     * @throws DBException
     */
    public List<Activity> getActivitiesByCategory(String categoryName, int userId) throws DBException {
        List<Activity> activities;
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            activities = dbManager.getActivitiesByCategory(connection, categoryName, userId);
            log.log(Level.DEBUG, "Got " + categoryName + " activities for user with " + userId + " id");
            connection.commit();
            log.log(Level.DEBUG, "Committed getting " + categoryName + " activities for user with " + userId + " id");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot get " + categoryName + " activities for user with " + userId + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback getting" + categoryName + " tasks for user with " + userId + " id");
            throw new DBException("Cannot get categories by name", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
        return activities;
    }

    /**
     * Marks activity with id in parameter as completed
     * by writing in DB time, when activity was completed
     * @param activityId
     * @param currentDate
     * @return
     * @throws DBException
     */
    public int completeActivity(String activityId, String currentDate) throws DBException {
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        int updatedRows;
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            updatedRows = dbManager.completeActivity(connection, activityId, currentDate);
            log.log(Level.DEBUG, "Completed activity with " + activityId + " id");
            connection.commit();
            log.log(Level.DEBUG, "Committed completing task with " + activityId + " id");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot complete task with " + activityId + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback completing activity with " + activityId + " id");
            throw new DBException("Cannot complete activity", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
        return updatedRows;
    }

    /**
     * Assigns activity for list of users in params. If boolean "IsImportant"
     * equals true than this activity will be marked as important in DB
     * @param activity
     * @param users
     * @param isImportant
     * @throws DBException
     */
    public void assignActivity(Activity activity, List<User> users, boolean isImportant) throws DBException {
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            dbManager.assignActivity(connection, activity, users, isImportant);
            connection.commit();
            log.log(Level.DEBUG, "Committed assigning task with users");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot assign task with " + activity.getActivityId() + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback assigning activity with " + activity.getActivityId() + " id");
            throw new DBException("Cannot assign activity", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
    }

    /**
     * Creates requested activity in DB for user in parameter
     * @param activity
     * @param user
     * @throws DBException
     */
    public void requestTask(Activity activity, User user) throws DBException {
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            dbManager.requestActivity(connection, activity, user);
            connection.commit();
            log.log(Level.DEBUG, "Committed requesting task with " + activity.getActivityName() + " id with user with " + user.getUserId() + " id");
        } catch (SQLException e) {
            log.log(Level.ERROR, "Cannot request task from user with " + user.getUserId() + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback requesting activity");
            e.printStackTrace();
            throw new DBException("Cannot request activity", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
    }

    /**
     * Marks activity as confirm if admin allows to do this requested task
     * @param activityId
     * @throws DBException
     */
    public void confirmActivity(String activityId) throws DBException {
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            dbManager.confirmActivity(connection, activityId);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot confirm task from user with " + activityId + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback confirming activity");
            throw new DBException("Cannot confirm activity", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
    }
    /**
     * Marks activity as rejected if admin not allows to do this requested task
     * or in case when user cancelled requesting
     * @param activityId
     * @throws DBException
     */
    public void rejectActivity(String activityId) throws DBException {
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        try {
            connection = DBUtils.getConnection();
            log.log(Level.DEBUG, "Got connection" + connection);
            dbManager.rejectActivity(connection, activityId);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Cannot reject task from user with " + activityId + " id");
            dbManager.rollback(connection);
            log.log(Level.ERROR, "Rollback rejecting activity");
            throw new DBException("Cannot reject activity", e);
        } finally {
            dbManager.close(connection);
            log.log(Level.DEBUG, "Closed connection " + connection);
        }
    }

    /**
     * Gets requested activities from DB for user with id in parameter
     * @param userId
     * @return
     * @throws DBException
     */
    public List<Activity> getRequestedActivities(int userId) throws DBException {
        List<Activity> activities;
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            activities = dbManager.getRequestedActivities(connection, userId);
            log.log(Level.DEBUG, "Got requested activities");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Can not get requested activities");
            throw new DBException("Can not get requested activities", e);
        } finally {
            dbManager.close(connection);
        }
        return activities;
    }

    /**
     * Gets all requested activities for admin
     * @return
     * @throws DBException
     */
    public List<Activity> getAllRequestedActivities() throws DBException {
        List<Activity> activities;
        Connection connection = null;
        DBManager dbManager = DBManager.getInstance();
        log.log(Level.DEBUG, "Got instance of DBManager");
        try {
            connection = DBUtils.getConnection();
            activities = dbManager.getAllRequestedActivities(connection);
            log.log(Level.DEBUG, "Got all requested activities");
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.ERROR, "Can not get all requested activities");
            throw new DBException("Can not get requested activities", e);
        } finally {
            dbManager.close(connection);
        }
        return activities;
    }
}
