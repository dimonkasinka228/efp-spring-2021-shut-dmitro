package com.my.DB;

import com.my.DB.Entity.User;
import com.my.Util.DBException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserManager {

    private static UserManager instance;

    private UserManager() {
        //empty body
    }

    public static synchronized UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    /**
     * Register user in DB
     * @param user
     * @throws DBException
     */
    public void registerUser(User user) throws DBException {
        Connection con = null;
        DBManager dbManager = DBManager.getInstance();
        try {
            con = DBUtils.getConnection();
            dbManager.registerUser(con, user);
            con.commit();
        } catch (SQLException ex) {
            dbManager.rollback(con);
            throw new DBException("Cannot register user", ex);
        } finally {
            dbManager.close(con);
        }
    }

    /**
     * Return user with login in parameter
     * @param login
     * @return
     * @throws DBException
     */
    public User getUser(String login) throws DBException {
        User user;
        Connection con = null;
        DBManager dbManager = DBManager.getInstance();
        try {
            con = DBUtils.getConnection();
            user = dbManager.getUser(con, login);
            con.commit();
        } catch (SQLException ex) {
            dbManager.rollback(con);
            throw new DBException("Cannot do getUser", ex);
        } finally {
            dbManager.close(con);
        }
        return user;
    }

    /**
     * Return list of all users
     * @return
     * @throws DBException
     */
    public List<User> getAllUsers() throws DBException {
        List<User> users;
        Connection con = null;
        DBManager dbManager = DBManager.getInstance();

        try {
            con = DBUtils.getConnection();
            users = dbManager.getAllUsers(con);
            con.commit();
        } catch (SQLException e) {
            dbManager.rollback(con);
            throw new DBException("Cannot get all users", e);
        } finally {
            dbManager.close(con);
        }
        return users;
    }
}
