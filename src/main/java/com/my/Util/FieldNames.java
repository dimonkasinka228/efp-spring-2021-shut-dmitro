package com.my.Util;

public class FieldNames {
    public static final String USER_ID= "user_id";
    public static final String USER_LOGIN = "user_login";
    public static final String USER_NAME = "user_name";
    public static final String USER_SURNAME = "user_last_name";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_ROLE_ID = "user_role_id";

    public static final String ACTIVITY_ID = "activity_id";
    public static final String ACTIVITY_NAME = "activity_name";
    public static final String ACTIVITY_DEADLINE = "activity_deadline";
    public static final String ACTIVITY_CATEGORY_ID = "activity_category_id";
    public static final String ACTIVITY_END_TIME = "activity_end_time";

    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";

    public static final String ROLE_ID = "role_id";
    public static final String ROLE_NAME = "role_name";

    public static final String USER_ACTIVITY_ID = "user_activity_id";
    public static final String USER_ACTIVITY_USER_ID = "a_user_id";
    public static final String USER_ACTIVITY_ACTIVITY_ID = "a_activity_id";
    public static final String CUSTOM_CATEGORY_ID = "custom_category_id";
    public static final String CUSTOM_CATEGORY_NAME = "custom_category_name";
    public static final String CUSTOM_CATEGORY_USER_ID = "user_id";
}
