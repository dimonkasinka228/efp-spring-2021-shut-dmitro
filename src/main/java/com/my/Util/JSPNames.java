package com.my.Util;

public class JSPNames {
    public static final String ERROR_JSP = "Error.jsp";
    public static final String LOGIN_PAGE_JSP = "LoginPage.jsp";
    public static final String MAIN_PAGE_JSP = "MainPage.jsp";
    public static final String ASSIGN_ACTIVITY_PAGE_JSP = "AssignTaskPage.jsp";
    public static final String TASK_REQUESTS_PAGE_JSP = "RequestActivityPage.jsp";
    public static final String REGISTER_PAGE_JSP = "RegisterPage.jsp";
}

