package com.my.Util;

import com.my.Commands.*;

import java.util.HashMap;
import java.util.Map;

public class CommandContainer {
    private static final Map<String, Command> commands;

    static {
        commands = new HashMap<>();
        commands.put("logging", new LoginCommand());
        commands.put("taskType", new TaskTypeCommand());
        commands.put("assignActivity", new AssignActivityCommand());
        commands.put("prepareAssigningInfo", new PrepareAssigningInfo());
        commands.put("completeTask", new CompleteTaskCommand());
        commands.put("leaveAccount", new LeaveAccountCommand());
        commands.put("requestActivity", new RequestActivityCommand());
        commands.put("answerRequest", new AnswerRequestCommand());
        commands.put("categoryFilter", new CategoryFilterCommand());
        commands.put("registration", new RegisterCommand());
        commands.put("requestTask", new PrepareRequestingInfoCommand());
        commands.put("changeLocale", new ChangeLocaleCommand());
    }

    public static Command getCommand(String commandName) {
        return commands.get(commandName);
    }
}
