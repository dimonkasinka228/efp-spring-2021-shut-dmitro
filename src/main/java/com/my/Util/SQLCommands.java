package com.my.Util;

public class SQLCommands {
    public static final String FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE user_login=?;";
    public static final String FIND_ACTIVITIES_BY_CATEGORY = "SELECT * FROM activities a JOIN user_activity ua ON a.activity_id = ua.activity_id JOIN categories c on c.category_id = a.activity_category_id JOIN custom_categories cc ON a.custom_category_id = cc.custom_category_id WHERE ua.user_id = ? AND c.category_name = ? and a.activity_end_time IS NULL ORDER BY a.activity_end_time;";
    public static final String FIND_COMPLETED_ACTIVITIES = "SELECT * FROM activities a JOIN user_activity ua ON a.activity_id = ua.activity_id JOIN categories c on c.category_id = a.activity_category_id JOIN custom_categories cc ON a.custom_category_id = cc.custom_category_id WHERE ua.user_id = ? AND a.activity_end_time IS NOT NULL ORDER BY a.activity_end_time;";
    public static final String FIND_REQUESTED_ACTIVITIES = "SELECT * FROM activities a JOIN user_activity ua ON a.activity_id = ua.activity_id JOIN categories c on c.category_id = a.activity_category_id JOIN custom_categories cc ON a.custom_category_id = cc.custom_category_id WHERE c.category_name = 'Requested' AND ua.user_id = ? ORDER BY a.activity_end_time;";
    public static final String FIND_ALL_REQUESTED_ACTIVITIES = "SELECT * FROM activities a JOIN user_activity ua ON a.activity_id = ua.activity_id JOIN categories c on c.category_id = a.activity_category_id JOIN custom_categories cc ON a.custom_category_id = cc.custom_category_id WHERE c.category_name = 'Requested' ORDER BY a.activity_end_time;";
    public static final String GET_ALL_USERS = "SELECT * FROM users;";
    public static final String COMPLETE_ACTIVITY = "UPDATE activities SET activity_end_time = ? WHERE activity_id = ?;";
    public static final String INSERT_ACTIVITY = "INSERT INTO activities (activity_name, activity_deadline, activity_category_id, custom_category_id) VALUES (?, ?, ?, ?);";
    public static final String GET_LATEST_ACTIVITY = "SELECT a.activity_id FROM activities a ORDER BY a.activity_id DESC LIMIT 1;";
    public static final String ASSIGN_ACTIVITY_WITH_USER = "INSERT INTO user_activity (user_id, activity_id) VALUES (?, ?);";
    public static final String REQUEST_ACTIVITY = "INSERT INTO activities (activity_name, activity_deadline, activity_category_id, custom_category_id) VALUES (?, ?, ?, ?);";
    public static final String CONFIRM_ACTIVITY = "UPDATE activities SET activity_category_id = '3' WHERE (activity_id = ?);";
    public static final String REJECT_ACTIVITY = "DELETE FROM activities WHERE (activity_id = ?);";
    public static final String REMOVE_ASSIGNMENTS = "DELETE FROM user_activity WHERE (activity_id = ?);";
    public static final String GET_CUSTOM_CATEGORIES = "SELECT * FROM time_organisation.custom_categories;";
    public static final String GET_CUSTOM_CATEGORY_ID = "SELECT * FROM custom_categories WHERE custom_category_name = ?;";
    public static final String REGISTER_USER = "INSERT INTO `time_organisation`.`users` (user_name, user_last_name, user_password, user_login, user_role_id) VALUES (?, ?, ?, ?, ?);";
    public static final int PAGINATION_OFFSET = 10;
}