package com.my.Filters;

import javax.servlet.*;
import java.io.IOException;


public class EncodingFilter implements Filter {
    private static final String ENCODING = "UTF-8";
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {
        String requestEncoding = req.getCharacterEncoding();
        if (requestEncoding == null) {
            req.setCharacterEncoding(ENCODING);
        }
        chain.doFilter(req, resp);
    }
}
