package com.my.Filters;


import com.my.DB.Entity.User;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserAccessFilter implements Filter {
    Logger log = LogManager.getLogger(UserAccessFilter.class);
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        User loggedUser = (User) req.getSession().getAttribute("loggedUser");
        if (loggedUser.getRoleId() != 1) {
            log.log(Level.DEBUG, "User role id = " + loggedUser.getRoleId());
            req.getSession().setAttribute("errorMessage", "Access not allowed");
            req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
            req.getRequestDispatcher(JSPNames.ERROR_JSP).forward(req, resp);
            return;
        }
        filterChain.doFilter(req, resp);
    }
}
