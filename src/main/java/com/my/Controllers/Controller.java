package com.my.Controllers;

import com.my.Commands.Command;
import com.my.Util.CommandContainer;
import com.my.Util.DBException;
import com.my.Util.JSPNames;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class that implements all the processing of requests from browser
 * to server by executing command, that comes from requests. Request should
 * contain name of command as unique key
 */
public class Controller extends HttpServlet {
    private static final Logger log = LogManager.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String address = JSPNames.ERROR_JSP;
        String commandName = req.getParameter("command");
        log.log(Level.DEBUG, "Command is " + commandName);
        Command command = CommandContainer.getCommand(commandName);
        log.log(Level.DEBUG, "Got command " + commandName);
        try {
            if (command != null) {
                address = command.execute(req, resp);
                log.log(Level.DEBUG, "Executed command " + commandName);
            } else {
                req.getSession().setAttribute("errorMessage", "This page does not exist");
                req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
                log.log(Level.ERROR, "Cannot execute command " + commandName + ". This command does not exist");
                req.getRequestDispatcher(JSPNames.ERROR_JSP).forward(req, resp);
                throw new DBException("Cannot execute command " + commandName + ". This command does not exist", new IllegalStateException());
            }
        } catch (DBException e) {
            log.log(Level.ERROR, "Cannot execute command " + commandName);
            e.printStackTrace();
        }
        req.getRequestDispatcher(address).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String address = JSPNames.ERROR_JSP;
        String commandName = req.getParameter("command");
        log.log(Level.DEBUG, "Command is " + commandName);
        Command command = CommandContainer.getCommand(commandName);
        log.log(Level.DEBUG, "Got command " + commandName);
        try {
            if (command != null) {
                address = command.execute(req, resp);
                log.log(Level.DEBUG, "Executed command " + commandName);
            } else {
                req.getSession().setAttribute("errorMessage", "This page does not exist");
                req.getSession().setAttribute("returnLink", JSPNames.MAIN_PAGE_JSP);
                log.log(Level.ERROR, "Cannot execute command " + commandName + ". This command does not exist");
                req.getRequestDispatcher(JSPNames.ERROR_JSP).forward(req, resp);
                throw new DBException("Cannot execute command " + commandName + ". This command does not exist", new IllegalStateException());
            }
        } catch (DBException e) {
            log.log(Level.ERROR, "Cannot execute command " + commandName);
            e.printStackTrace();
        }
        resp.sendRedirect(address);
    }
}