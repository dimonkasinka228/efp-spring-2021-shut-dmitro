package com.my.Listeners;

import com.my.DB.DBManager;
import com.my.DB.DBUtils;
import com.my.Util.CommandContainer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        DBUtils.init();
        initCommandsContainer();
    }

    public void initCommandsContainer() {
        String path = CommandContainer.class.getName();
        try {
            Class.forName(path);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
