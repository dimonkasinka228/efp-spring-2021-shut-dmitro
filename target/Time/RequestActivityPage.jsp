<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale" value="${not empty sessionScope.locale ? sessionScope.locale : 'en'}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Request Task</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bs5/css/bootstrap.css">
</head>
<body>
<div>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <input type="hidden" name="command" value="requestActivity">
    <div class="col-md-6">
        <label for="validationDefault01" class="form-label"><fmt:message key="requesttask.taskdescription"/></label>
        <input type="text" class="form-control" name="activityName" id="validationDefault01" placeholder="<fmt:message key="requesttask.taskdescription"/>" required>
    </div>
    <div class="col-md-6">
        <label for="activityEnd" class="form-label"><fmt:message key="requesttask.deadline"/></label>
        <input type="datetime-local" name="activityEnd" id="activityEnd" min="${requestScope.currentDate}" required>
    </div>
    <div class="col-12">
        <button class="btn btn-primary" type="submit"><fmt:message key="requesttask.submit"/></button>
    </div>
</form>
</div>
<div>
    <form action="${pageContext.request.contextPath}/MainPage.jsp">
        <button class="btn btn-primary" type="submit"><fmt:message key="requesttask.cancel"/></button
    </form>
</div>
<script src="https://unpkg.com/@popperjs/core@2.4.0/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bs5/js/bootstrap.js"></script>
</body>
</html>
