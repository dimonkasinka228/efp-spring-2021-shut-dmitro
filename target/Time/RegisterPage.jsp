<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale" value="${not empty sessionScope.locale ? sessionScope.locale : 'en'}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bs5/css/bootstrap.css">
    <title>Registration</title>
</head>
<body>
<script src="https://unpkg.com/@popperjs/core@2.4.0/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bs5/js/bootstrap.js"></script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-4">
                <div class="register-form bg-light mt-4 p-5">
                    <form action="${pageContext.request.contextPath}/controller" method="post" class="row g-3">
                        <input type="hidden" name="command" value="registration">
                        <h4><fmt:message key="register.registration"/></h4>
                        <div class="col-12">
                            <label><fmt:message key="register.username"/></label>
                            <input type="text" name="username" class="form-control" placeholder="Your name" required>
                        </div>
                        <div class="col-12">
                            <label><fmt:message key="register.userlastname"/></label>
                            <input type="text" name="lastname" class="form-control" placeholder="Your lastname" required>
                        </div>
                        <div class="col-12">
                            <label><fmt:message key="register.userlogin"/></label>
                            <input type="text" name="login" class="form-control" placeholder="Your login" required>
                        </div>
                        <div class="col-12">
                            <label><fmt:message key="register.userpassword"/></label>
                            <input type="password" name="password" class="form-control" placeholder="Your password" required>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark float-end"><fmt:message key="register.button"/></button>
                        </div>
                    </form>
                    <hr class="mt-4">
                    <div class="col-12">
                        <p class="text-center mb-0"><fmt:message key="register.question"/> <a href="${pageContext.request.contextPath}/LoginPage.jsp"><fmt:message key="register.link"/></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
