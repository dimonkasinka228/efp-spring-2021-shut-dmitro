<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale" value="${not empty sessionScope.locale ? sessionScope.locale : 'en'}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources"/>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bs5/css/bootstrap.css">
    <title>Login Page</title>
</head>
<body>
<script src="https://unpkg.com/@popperjs/core@2.4.0/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bs5/js/bootstrap.js"></script>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-4">
            <div class="login-form bg-light mt-4 p-4">
                <form action="${pageContext.request.contextPath}/controller" method="get" class="row g-3">
                    <input type="hidden" value="logging" name="command">
                    <h4>Welcome Back</h4>
                    <div class="col-12">
                        <label><fmt:message key="login.username"/></label>
                        <input type="text" name="login" class="form-control" placeholder="Username">
                    </div>
                    <div class="col-12">
                        <label><fmt:message key="login.passsword"/></label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-dark float-end"><fmt:message key="login.button"/></button>
                    </div>
                </form>
                <hr class="mt-4">
                <div class="col-12">
                    <p class="text-center mb-0"><fmt:message key="login.question"/><a href="${pageContext.request.contextPath}/RegisterPage.jsp"><fmt:message key="login.linktext"/></a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
