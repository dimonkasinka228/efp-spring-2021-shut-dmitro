<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>
    ${sessionScope.errorMessage.toString()}
</h1>
<c:if test="${sessionScope.returnLink == null}">
    <form action="${pageContext.request.contextPath}/LoginPage.jsp">
        <input type="submit" value="Back">
    </form>
</c:if>
<c:if test="${sessionScope.returnLink != null}">
    <form action="${pageContext.request.contextPath}/${sessionScope.returnLink}">
        <input type="submit" value="Back">
    </form>
</c:if>
</body>
</html>
