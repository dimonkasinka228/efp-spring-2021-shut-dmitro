<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale" value="${not empty sessionScope.locale ? sessionScope.locale : 'en'}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources"/>
<html>
<head>
    <title>Main Page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bs5/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>

    <script
            src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
<script src="https://unpkg.com/@popperjs/core@2.4.0/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bs5/js/bootstrap.js"></script>
<div class="app">
    <div class="container-fluid">
        <div class="row">
<%--            <t:customtag/>--%>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#"><fmt:message key="mainpage.navigation"/></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                        <div class="btn-group" role="group">
                            <c:if test="${sessionScope.loggedUser.roleId == 1}">
                                <form action="${pageContext.request.contextPath}/controller" method="get">
                                    <input type="hidden" name="command" value="prepareAssigningInfo">
                                    <input type="submit" class="btn-light" value="<fmt:message key="mainpage.assign"/>">
                                </form>
                                <form action="${pageContext.request.contextPath}/controller" method="get">
                                    <input type="hidden" name="command" value="taskType">
                                    <input type="hidden" name="taskType" value="Requested">
                                    <input type="submit" class="btn-light"
                                           value="<fmt:message key="mainpage.taskrequests"/>">
                                </form>
                            </c:if>
                            <c:if test="${sessionScope.loggedUser.roleId == 2}">
                                <form action="${pageContext.request.contextPath}/controller" method="get">
                                    <input type="hidden" name="command" value="requestTask">
                                    <input type="submit" class="btn-light"
                                           value="<fmt:message key="mainpage.requesttask"/>">
                                </form>
                            </c:if>
                            <form action="${pageContext.request.contextPath}/controller" method="get">
                                <input type="hidden" name="command" value="leaveAccount">
                                <input type="submit" class="btn-light" value="<fmt:message key="mainpage.logout"/>">
                            </form>
                            <a class="btn bg-light" type="button"
                               href="controller?command=changeLocale&locale=ru">RU</a>
                            <a class="btn btn-light" type="button"
                               href="controller?command=changeLocale&locale=en">EN</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="btn-group-vertical" role="group">
                    <form action="${pageContext.request.contextPath}/controller" method="get">
                        <input type="hidden" name="command" value="taskType">
                        <input type="hidden" name="taskType" value="Important">
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="mainpage.important"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/controller" method="get">
                        <input type="hidden" name="command" value="taskType">
                        <input type="hidden" name="taskType" value="Appointed">
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="mainpage.appointed"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/controller" method="get">
                        <input type="hidden" name="command" value="taskType">
                        <input type="hidden" name="taskType" value="Requested">
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="mainpage.requested"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/controller" method="get">
                        <input type="hidden" name="command" value="taskType">
                        <input type="hidden" name="taskType" value="My tasks">
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="mainpage.mytasks"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/controller" method="get">
                        <input type="hidden" name="command" value="taskType">
                        <input type="hidden" name="taskType" value="Completed">
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="mainpage.completed"/>">
                    </form>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-6">
                        <form action="${pageContext.request.contextPath}/controller" method="get">
                            <input type="hidden" name="command" value="categoryFilter">
                            <select name="categoryFilter" id="category_filter">
                                <c:forEach var="category" items="${sessionScope.customCategories}">
                                    <option>${category.customCategoryName}</option>
                                </c:forEach>
                            </select>
                            <input type="submit" value="<fmt:message key="mainpage.filtrate"/>">
                        </form>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-striped table-hover" id="myTable">
                        <thead>
                        <tr>
                            <th><fmt:message key="mainpage.name"/></th>
                            <th><fmt:message key="mainpage.category"/></th>
                            <c:if test="${sessionScope.taskType != 'Completed'}">
                                <th><fmt:message key="mainpage.deadline"/></th>
                                <th><fmt:message key="mainpage.action1"/></th>
                            </c:if>
                            <c:if test="${sessionScope.loggedUser.roleId == 1 and sessionScope.taskType == 'Requested'}">
                                <th><fmt:message key="mainpage.action2"/></th>
                            </c:if>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="task" items="${sessionScope.taskList}">
                            <tr>
                                <td>${task.activityName}</td>
                                <td>${task.customCategoryName}</td>
                                <c:if test="${task.activityEndTime == null}">
                                    <td>${task.activityDeadline}</td>
                                </c:if>
                                <c:if test="${task.activityEndTime == null and sessionScope.loggedUser.roleId == 2 and task.activityCategoryId != 4}">
                                    <td>
                                        <form action="${pageContext.request.contextPath}/controller" method="post">
                                            <input type="hidden" name="command" value="completeTask">
                                            <input type="hidden" name="endedTaskId" value="${task.activityId}">
                                            <input type="submit" value="<fmt:message key="mainpage.complete"/>">
                                        </form>
                                    </td>
                                </c:if>
                                <c:if test="${task.activityCategoryId == 4 and sessionScope.loggedUser.roleId == 1}">
                                    <td>
                                        <form action="${pageContext.request.contextPath}/controller" method="post">
                                            <input type="hidden" name="command" value="answerRequest">
                                            <input type="hidden" name="endedTaskId" value="${task.activityId}">
                                            <input type="hidden" name="answer" value="confirm">
                                            <input type="submit" value="<fmt:message key="mainpage.confirmtask"/>">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="${pageContext.request.contextPath}/controller" method="post">
                                            <input type="hidden" name="command" value="answerRequest">
                                            <input type="hidden" name="endedTaskId" value="${task.activityId}">
                                            <input type="hidden" name="answer" value="reject">
                                            <input type="submit" value="<fmt:message key="mainpage.rejecttask"/>">
                                        </form>
                                    </td>
                                </c:if>
                                <c:if test="${task.activityCategoryId == 4 and sessionScope.loggedUser.roleId == 2}">
                                    <td>
                                        <form action="${pageContext.request.contextPath}/controller" method="post">
                                            <input type="hidden" name="command" value="answerRequest">
                                            <input type="hidden" name="endedTaskId" value="${task.activityId}">
                                            <input type="hidden" name="answer" value="reject">
                                            <input type="submit" value="<fmt:message key="mainpage.removetask"/>">
                                        </form>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>
</body>
</html>